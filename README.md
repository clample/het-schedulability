# Heterogeneous Computer Simulator

Simulates a heterogeneous computer serving a real time workload as described in `A Framework for Supporting Real-Time Applications on Dynamic Reconfigurable FPGAs` from Biondi et. al.

