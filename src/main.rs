use rand::{thread_rng, Rng};
use plotters::prelude::*;
use std::cmp;
mod simulation;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    plot(schedulability_analysis(5000), schedulability_analysis(10000))
}

fn schedulability_analysis(reconfiguration_speed: i32) -> Vec<(f32, f32)> {
    let mut schedulability: Vec<(f32, f32)> = Vec::new();
    let mut hw_utilization = 0.0;
    let sw_utilization = 0.1;
    while hw_utilization < 2.0 {
        let mut num_schedulable = 0;
        let sample_size = 100;
        for i in 0..sample_size {
            if run_sim(hw_utilization, sw_utilization, reconfiguration_speed) {
                num_schedulable += 1;
            }
        }
        schedulability.push((hw_utilization as f32, num_schedulable as f32 / sample_size as f32));
        hw_utilization += 0.05;
    }
    schedulability
}

fn plot(schedulability_1: Vec<(f32, f32)>, schedulability_2: Vec<(f32, f32)>) -> Result<(), Box<dyn std::error::Error>> {
    let root = BitMapBackend::new("0.png", (640, 480)).into_drawing_area();
    root.fill(&WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .caption("Feasibility", ("sans-serif", 30).into_font())
        .margin(10)
        .x_label_area_size(60)
        .y_label_area_size(60)
        .build_ranged(0f32..2f32,0f32..1f32)?;

    chart
        .configure_mesh()
        .y_desc("Schedulability Ratio")
        .x_desc("Hardware Utilization")
        .draw()?;

    chart
        .draw_series(LineSeries::new(
            schedulability_1,
            &RED,
        ).point_size(3))?
        .label("Reconfiguration Speed: 5000")
        .legend(|(x,y)| PathElement::new(vec![(x,y), (x + 20, y)], &RED));

    chart
        .draw_series(LineSeries::new(
            schedulability_2,
            &BLUE,
        ).point_size(3))?
        .label("Reconfiguration Speed: 10000")
        .legend(|(x,y)| PathElement::new(vec![(x,y), (x + 20, y)], &BLUE));
    
    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;
    Ok(())
}


fn run_sim(hw_utilization: f64, sw_utilization: f64, reconfiguration_speed: i32) -> bool {
    let partitions = vec![
        simulation::Partition::new(2,100000),
        simulation::Partition::new(2,100000),
        simulation::Partition::new(2,100000)
    ];
    let tasks = make_tasks(&partitions, hw_utilization, sw_utilization);
    let mut simulation = simulation::Simulation::new(partitions, tasks, reconfiguration_speed);
    simulation.execute()
}

fn uunifast(num_tasks: usize, target_utilization: f64) -> Vec<f64> {
    let mut sum_U = target_utilization;
    let mut utilizations = Vec::with_capacity(num_tasks);
    let mut rng = thread_rng();
    for i in 1..=num_tasks-1 {
        let next_sum_U: f64 = sum_U * rng.gen_range::<f64, f64, f64>(0.0,1.0).powf(1.0 / (num_tasks - i) as f64);
        utilizations.push(sum_U - next_sum_U);
        sum_U = next_sum_U;
    }
    utilizations.push(sum_U);
    
    utilizations
}

fn make_tasks(partitions: &Vec<simulation::Partition>, hw_utilization: f64, sw_utilization: f64) -> Vec<simulation::Task> {
    let mut tasks = Vec::new();
    let mut rng = thread_rng();
    let sw_utilizations = uunifast(9, sw_utilization);
    let hw_utilizations = uunifast(9, hw_utilization);

    let mut periods = get_periods(9);    
    // Each partition has 3 tasks
    for partition in 0..3 {
        for task in 0..3 {
            let index: usize = (partition * 3) + task;
            let period = periods[index];
            
            let utilization_subtask1 = rng.gen_range(0.0, sw_utilizations[index]);
            let utilization_subtask2 = sw_utilizations[index] - utilization_subtask1;
            let subtasks = vec![
                simulation::SubTask::Software {
                    wcet: get_wcet(period, utilization_subtask1),
                },
                simulation::SubTask::Hardware {
                    size: partitions[partition].slot_size,
                    wcet: get_wcet(period, hw_utilizations[index]),
                    affinity: partition,
                },
                simulation::SubTask::Software {
                    wcet: get_wcet(period, utilization_subtask2),
                }
            ];
            tasks.push(simulation::Task {
                subtasks,
                priority: get_priority(&periods, index), // TODO: Properly generate prio
                period: period,
                deadline: period,
            })
        }
    }
    tasks
}

fn get_wcet(period: i32, utilization: f64) -> i32 {
    cmp::max(1, (period as f64 * utilization) as i32)
}

// Sure we keep calling this O(n) function, but n is 9 so it's ok
fn get_priority(periods: &Vec<i32>, index: usize) -> i32 {
    let mut priority = 1;
    let period = periods[index];
    for i in 0..periods.len() {
        let other_period = periods[i];
        if (other_period == period && i < index) || (other_period < period) {
            priority += 1;
        }
    }
    priority
}

// See Goossens Macq, limitation of the hyperperiod
fn get_periods(num: usize) -> Vec<i32> {
    let primes: Vec<(i32,u32)> = vec![(2,5),(3,4),(5,4)];
    let mut periods = Vec::new();
    let mut rng = thread_rng();
    for i in 0..num {
        let mut period = 1;
        for (prime, max) in primes.iter() {
            period *= prime.pow(rng.gen_range(1,max));
        }
        periods.push(period)
    }

    periods
}
