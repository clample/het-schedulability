// TODO: Why use i32 everywhere? We need unsigned most of the time => u32 or usize
extern crate num_integer;

pub struct Simulation {
    tasks: Vec<Task>,
    active_jobs: Vec<Job>,
    partitions: Vec<Partition>,
    reconfiguration_speed: i32,
}

impl Simulation {
    
    pub fn new(partitions: Vec<Partition>, tasks: Vec<Task>, reconfiguration_speed: i32) -> Simulation {
        Simulation {
            tasks: tasks,
            active_jobs: Vec::new(),
            partitions,
            reconfiguration_speed: reconfiguration_speed,
        }
    }

    // Execute the simulation
    // Returns true if feasible
    pub fn execute(&mut self) -> bool {
        // TODO: How to calculate the end time? I think it should work to simply simulate until the end of the hyper period
        // The FRED paper uses a completely different approach to checking feasibility
        let end_time = self.get_hyperperiod();
        println!("Simulating until {}", end_time);
        for time in 0..=end_time {
            self.gen_jobs(time);
            if !self.check_deadlines(time) {
                println!("Failed at time {} {:#?}", time, self.active_jobs);
                return false;
            }
            self.execute_jobs();
        }

        true
    }

    fn get_hyperperiod(&self) -> i32 {
        self.tasks.iter().fold(1, |acc, task| 
            num_integer::lcm(acc, task.period)
        )     
    }

    fn check_deadlines(&mut self, time: i32) -> bool {
        // Remove completed subjobs from job
        for job in self.active_jobs.iter_mut() {
            let subjobs = &mut job.subjobs;
            let first_elem = subjobs.first();
            match first_elem {
                Some(SubJob::Hardware(hj)) => {
                    if hj.remaining_comp_time == 0 {
                        self.partitions[hj.affinity].occupied_slots -= 1;
                        subjobs.remove(0);
                    } else if hj.remaining_comp_time < 0 {
                        panic!("Unexpected negative computation time at {} {:#?}", time, self.tasks);
                    }
                },
                Some(SubJob::Software(sj)) => {
                    if sj.remaining_comp_time == 0 {
                        subjobs.remove(0);
                        job.submit_hardware_configuration_request(time);
                    } else if sj.remaining_comp_time < 0 {
                        panic!("Unexpected negative computation time at {} {:#?}", time, self.tasks);
                    }
                },
                None => {},
            }
        }
        
        // Remove completed jobs from active_jobs
        self.active_jobs.retain(|job| job.subjobs.len() != 0);
        
        // If any jobs are past their deadline, panic
        for job in self.active_jobs.iter() {
            if job.absolute_deadline == time {
                return false;
            }
        }
        true
    }

    fn gen_jobs(&mut self, time: i32) {
        for task in self.tasks.iter() {
            if task.is_due(time) {
                self.active_jobs.push(task.instantiate_job(time));
            }
        }
    }

    fn execute_jobs(&mut self) {
        // Execute highest priority SW job
        if let Some(mut subjob) = self.get_highest_prio_software_subjob() {
            subjob.remaining_comp_time -= 1;
        }
        
        // Execute all configured hw jobs
        for job in self.active_jobs.iter_mut() {
            match job.subjobs.first_mut() {
                Some(SubJob::Hardware(hj)) => {
                    if hj.is_configured() {
                        hj.remaining_comp_time -= 1;
                    }
                },
                _ => {},
            }
        }
    
        // reconfigure slot if needed
        self.execute_reconfiguration();
    }

    fn execute_reconfiguration(&mut self) {
        // We need to scan the active jobs to check which one needs to be configured
        // TODO: This code is pretty awful - fix this
        
        // If a job already started configuration, then we continue configuring iter
        // Since we have nonpreemptible FRI
        for job in self.active_jobs.iter_mut() {
            match job.subjobs.first_mut() {
                Some(SubJob::Hardware(hj)) => {
                    if hj.started_configuration && !hj.is_configured() {
                        hj.configuration_step(self.reconfiguration_speed);
                        return;
                    }
                },
                _ => {},
            }
        }
        
        // Otherwise no job has started configuration
        // We find the one with the earliest request time and start configuring it
        let mut first_prio_request: Option<&mut HardwareSubJob> = None;
        for job in self.active_jobs.iter_mut() {
            match job.subjobs.first_mut() {
                Some(SubJob::Hardware(hj)) => {
                    let partition = &self.partitions[hj.affinity];
                    if partition.is_full() {
                        continue;
                    }
                    match first_prio_request {
                        Some(first_request) => {
                            if hj.request_time.unwrap() < first_request.request_time.unwrap() {
                                first_prio_request = Some(hj);
                            } else {
                                // We need to do this since we've moved first_request
                                // Probably there's a cleaner way
                                first_prio_request = Some(first_request);
                            }
                        },
                        None => {
                            first_prio_request = Some(hj);
                        }
                    }
                },
                _ => {},
            }
        }
        
        match first_prio_request {
            Some(first_request) => {
                first_request.started_configuration = true;
                first_request.configuration_step(self.reconfiguration_speed);
                let partition = &mut self.partitions[first_request.affinity];
                partition.occupied_slots += 1;
            },
            _ => {}
            
        }   
    }


    fn get_highest_prio_software_subjob(&mut self) -> Option<&mut SoftwareSubJob> {
        let mut priority = 0;
        let mut subjob = Option::None;
        
        for job in self.active_jobs.iter_mut() {
            if subjob.is_none() || job.priority < priority {
                match job.subjobs.first_mut() {
                    Some(SubJob::Software(sj)) => {
                        priority = job.priority;
                        subjob = Option::Some(sj);
                    },
                    _ => {},
                }
            }
        }
        subjob
    }
  
}


#[derive(Debug)]
pub enum SubTask {
    Hardware {
        size: i32,
        wcet: i32,
        affinity: usize,
    },
    Software {
        wcet: i32,
    },
}

impl SubTask {
    fn instantiate_subjob(&self) -> SubJob {
        match self {
            SubTask::Software { wcet } => SubJob::Software(SoftwareSubJob {
                remaining_comp_time: *wcet,
            }),
            SubTask::Hardware {
                size,
                wcet,
                affinity,
            } => SubJob::Hardware(HardwareSubJob {
                size: *size,
                remaining_comp_time: *wcet,
                affinity: *affinity,
                // FIXME: A hw job must reconfigure the whole slot
                // So either size should be equal to the size of a slot
                // Or we need to get the remaining_configuration_blocks
                // using `affinity`.
                remaining_configuration_blocks: size.clone(),
                started_configuration: false,
                request_time: None,
            }),
        }
    }
}

pub struct Partition {
    num_slots: i32,
    pub slot_size: i32,
    occupied_slots: i32,
}

impl Partition {
    pub fn new(num_slots: i32, slot_size: i32) -> Partition {
        Partition {
            num_slots,
            slot_size,
            occupied_slots: 0,
        }
    }
    fn is_full(&self) -> bool {
        self.num_slots == self.occupied_slots
    }
}

#[derive(Debug)]
pub struct Task {
    pub subtasks: Vec<SubTask>,
    pub priority: i32,
    pub period: i32,
    pub deadline: i32,
}

impl Task {
    fn is_due(&self, time: i32) -> bool {
        time % self.period == 0
    }

    fn instantiate_job(&self, time: i32) -> Job {
        Job {
            absolute_deadline: self.deadline + time,
            priority: self.priority,
            subjobs: self
                .subtasks
                .iter()
                .map(|subtask| subtask.instantiate_subjob())
                .collect(),
        }
    }
}

#[derive(Debug)]
struct Job {
    absolute_deadline: i32,
    priority: i32,
    subjobs: Vec<SubJob>,
}

impl Job {
    fn submit_hardware_configuration_request(&mut self, time: i32) {
        match self.subjobs.first_mut() {
            Some(SubJob::Hardware(hj)) => {
                if hj.request_time.is_none() {
                    hj.request_time = Some(time);
                }
            },
            _ => {},
        }
    }
}

#[derive(Debug)]
enum SubJob {
    Hardware(HardwareSubJob),
    Software(SoftwareSubJob),
}

#[derive(Debug)]
struct SoftwareSubJob {
    remaining_comp_time: i32,
}

#[derive(Debug)]
struct HardwareSubJob {
    size: i32,
    remaining_comp_time: i32,
    affinity: usize,
    remaining_configuration_blocks: i32,
    request_time: Option<i32>,
    started_configuration: bool,
}

impl HardwareSubJob {
    fn is_configured(&self) -> bool {
        self.remaining_configuration_blocks == 0
    }

    fn configuration_step(&mut self, reconfig_speed: i32) {
        if self.remaining_configuration_blocks >= reconfig_speed {
            self.remaining_configuration_blocks -= reconfig_speed;
        } else {
            self.remaining_configuration_blocks = 0;
        }
    }
}

