let
  sources = import ./nix/sources.nix;
  rust = import ./nix/rust.nix { inherit sources; };
  pkgs = import sources.nixpkgs {};
in
pkgs.mkShell {
  buildInputs = [
    rust pkgs.gdb pkgs.gdbgui pkgs.cmake pkgs.pkg-config pkgs.freetype pkgs.file pkgs.expat
  ];
}
